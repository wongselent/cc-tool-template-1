# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'd:\1_DATA_KERJA\0_PROJECT\3_Python\render_tools\project_templates\{{cookiecutter.repo_name}}\package\resources\designer\mainwindow.ui'
#
# Created: Sun Jun 23 07:10:38 2019
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_main_window(object):
    def setupUi(self, main_window):
        main_window.setObjectName("main_window")
        main_window.resize(297, 118)
        self.centralwidget = QtGui.QWidget(main_window)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.main_layout = QtGui.QVBoxLayout()
        self.main_layout.setObjectName("main_layout")
        self.open_widget_btn = QtGui.QPushButton(self.centralwidget)
        self.open_widget_btn.setObjectName("open_widget_btn")
        self.main_layout.addWidget(self.open_widget_btn)
        self.verticalLayout_2.addLayout(self.main_layout)
        main_window.setCentralWidget(self.centralwidget)

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslateUi(self, main_window):
        main_window.setWindowTitle(QtGui.QApplication.translate("main_window", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.open_widget_btn.setText(QtGui.QApplication.translate("main_window", "Open Widget", None, QtGui.QApplication.UnicodeUTF8))

