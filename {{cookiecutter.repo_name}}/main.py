import sys
import os 
import platform

sys.dont_write_bytecode = True

from PySide.QtGui import QApplication

MAIN_PATH = os.path.dirname(__file__)
PALETTE_FILEPATH = os.path.join(MAIN_PATH, 'libs/theme', 'qpalette_maya2016.json')

# if MAIN_PATH not in sys.path:
#     sys.path.append(MAIN_PATH)

import {{cookiecutter.repo_name}} as {{cookiecutter.repo_name}}
import libs.theme as theme

def _mayaMainWindow():
    for obj in QApplication.topLevelWidgets():
        if obj.objectName() == 'MayaWindow':
            return obj
    raise RuntimeError('Could not find MayaWindow instance')


def run_maya():
    reload({{cookiecutter.repo_name}})
    global win
    
    try:
        win.close()
    except:
        pass

    #TODO: Ganti variable class
    win = {{cookiecutter.repo_name}}.{{cookiecutter.app_class_name}}(parent=_mayaMainWindow())
    win.setProperty("saveWindowPref", True)
    win.show()
    win.raise_()

def run_std():
    app = QApplication(sys.argv)
    if not platform.system() == 'Darwin':
        theme.mayapalette.set_maya_palette_with_tweaks(PALETTE_FILEPATH)
    win = {{cookiecutter.repo_name}}.{{cookiecutter.app_class_name}}()
    win.show()
    sys.exit(app.exec_())