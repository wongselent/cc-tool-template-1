import sys
import os

sys.dont_write_bytecode = True

from PySide.QtGui import QMainWindow, QWidget
from PySide.QtCore import Qt

import gui.ui_mainwindow as ui_mainwindow

class {{cookiecutter.app_class_name}}(QMainWindow, ui_mainwindow.Ui_main_window):
    def __init__(self, parent=None):
        super(self.__class__, self).__init__(parent)
        self.setupUi(self)
        self.initUi()
    
    def initUi(self):
        self.setWindowTitle = '{{cookiecutter.app_title}}'
        pass